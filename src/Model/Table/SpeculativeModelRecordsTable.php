<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

class SpeculativeModelRecordsTable extends Table {
	
	public function initialize(array $config) {
		$this->belongsTo("SpeculativeModels");
	}
	
	public function validationDefault(Validator $validator) {
		return $validator
		->requirePresence("speculative_model_id", null, "You need to provide Model ID")
		->notEmpty("speculative_model_id", "You need to provide Model ID")
		->add("speculative_model_id", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Model ID",
			],
			"naturalNumber" => [
				"rule" => "naturalNumber",
				"message" => "You must provide a valid Model ID"
			]
		])
		
		->requirePresence("date_of_last_rate", null, "You need to provide Date of Last Rate")
		->notEmpty("date_of_last_rate", "You need to provide Date of Last Rate")
		->add("date_of_last_rate", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Date of Last Rate",
			],
			"date" => [
				"rule" => "date",
				"message" => "You need to provide a valid Date of Last Rate",
			],
		])
		
		->requirePresence("last_rate", null, "You need to provide Last Rate")
		->notEmpty("last_rate", "You need to provide Last Rate")
		->add("last_rate", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Last Rate",
			],
			"numeric" => [
				"rule" => "numeric",
				"message" => "You need to provide a valid Last Rate",
			],
		])
		
		->requirePresence("predicted_state", null, "You need to provide Predicted State")
		->notEmpty("predicted_state", "You need to provide Predicted State")
		->add("predicted_state", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Predicted State",
			],
			"numeric" => [
				"rule" => "numeric",
				"message" => "You need to provide a valid Predicted State",
			],
		])
		
		->requirePresence("prediction_confidence", null, "You need to provide Prediction Confidence")
		->notEmpty("prediction_confidence", "You need to provide Prediction Confidence")
		->add("prediction_confidence", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Prediction Confidence",
			],
			"numeric" => [
				"rule" => "numeric",
				"message" => "You need to provide a valid Prediction Confidence",
			],
		])
		
		->notEmpty("actual_rate", "You need to provide Actual State")
		->add("actual_rate", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Actual State",
			],
			"numeric" => [
				"rule" => "numeric",
				"message" => "You need to provide a valid Actual State",
			],
		])
		
		->notEmpty("result", "You need to provide Result")
		->add("result", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide Result",
			],
			"boolean" => [
				"rule" => "boolean",
				"message" => "You need to provide a valid Result",
			],
		])
		;
	}
	
	public function buildRules(RulesChecker $rules) {
		return $rules;
	}
	
}