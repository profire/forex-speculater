<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

class SpeculativeModelsTable extends Table {
	
	public function initialize(array $config) {
		
	}
	
	public function validationDefault(Validator $validator) {
		return $validator
		->requirePresence("name", null, "You need to provide name")
		->notEmpty("name", "You need to provide name")
		->add("name", [
			"notBlank" => [
				"rule" => "notBlank",
				"message" => "You need to provide name",
			],
		])
		;
	}
	
	public function buildRules(RulesChecker $rules) {
		return $rules;
	}
	
	
}