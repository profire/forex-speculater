<?php
namespace App\Controller;

use function GuzzleHttp\json_encode;
use function GuzzleHttp\json_decode;
use Cake\Chronos\Chronos;
use Cake\Core\Configure;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;

class ForexController extends AppController {
	private $serviceName = "forex-speculator-daily";
	
	public function initialize() {
		parent::initialize();
		
		$this->loadComponent('XeCurrency');
		
		$this->loadModel("SpeculativeModels");
		$this->loadModel("SpeculativeModelRecords");
	}
	
	public function index() {
		
	}
	
	public function startTraining() {
		//Async Init: Create Service
		try {
			$promiseToDedeCreateService = $this->dedeClient->postAsync("DeepDetect/servicesCreate.json", [
				"form_params" => [
					"service-name" => $this->serviceName,
					"nclasses" => 3,
				],
			]);
			/*DEBUG*/ //pr($promiseToDedeCreateService->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Create Service
		$responseToDedeCreateService = json_decode($promiseToDedeCreateService->wait()->getBody()->getContents(), true);
		/*DEBUG*/ //pr($responseToDedeCreateService);//exit;
		
		
		
		


		//Async Init: Preparing Header
		try {
			$promiseToDedetrainingPrepHeader = $this->dedeClient->postAsync("DeepDetect/trainingPrepHeader.json", [
				"form_params" => [
					"headers" => "day1,day2,day3,day4,day5,day6,day7",
					"label" => "day7",
				],
			]);
			/*DEBUG*/ //pr($promiseToDedetrainingPrepHeader->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Create Service
		$responseToDedetrainingPrepHeader = json_decode($promiseToDedetrainingPrepHeader->wait()->getBody()->getContents(), true);
		/*DEBUG*/ //pr($responseToDedetrainingPrepHeader);exit;
		
		
		
		

		$rates = $this->XeCurrency->getHistoricRates("USD", "SGD");
		foreach ($rates["list"]["86400"]["rates"] as $key => $rate) {
			//If there is no seventh day, it has reach the end.
			if (!isset($rates["list"]["86400"]["rates"][$key+6])) {
				break;
			}
			
			//Measure if the sixth to seventh day is an increase or decrease
			//0 = decrease, 1 = equal, 2 = increase
			if ($rates["list"]["86400"]["rates"][$key+5]["rate"] > $rates["list"]["86400"]["rates"][$key+6]["rate"]) {
				$day7 = 0;
			} else if ($rates["list"]["86400"]["rates"][$key+5]["rate"] == $rates["list"]["86400"]["rates"][$key+6]["rate"]) {
				$day7 = 1;
			} else {
				$day7 = 2;
			}
			
			//CSV Column
			$data = $rates["list"]["86400"]["rates"][$key]["rate"].",".
					$rates["list"]["86400"]["rates"][$key+1]["rate"].",".
					$rates["list"]["86400"]["rates"][$key+2]["rate"].",".
					$rates["list"]["86400"]["rates"][$key+3]["rate"].",".
					$rates["list"]["86400"]["rates"][$key+4]["rate"].",".
					$rates["list"]["86400"]["rates"][$key+5]["rate"].",".
					$day7;
			
			//Async Init: Preparing Data
			try {
				$promiseToDedetrainingPrepData = $this->dedeClient->postAsync("DeepDetect/trainingPrepData.json", [
					"form_params" => [
						"header-id" => $responseToDedetrainingPrepHeader["id"],
						"data" => $data,
					],
				]);
				/*DEBUG*/ //pr($promiseToDedetrainingPrepData->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
			
			//Async Result: Create Service
			$responseToDedetrainingPrepData = json_decode($promiseToDedetrainingPrepData->wait()->getBody()->getContents(), true);
			/*DEBUG*/ //pr($responseToDedetrainingPrepData);//exit;
		}
		
		
		
		
		







		//Async Init: Start Training
		try {
			$promiseToDedetrainingStart = $this->dedeClient->postAsync("DeepDetect/trainingStart.json", [
				"form_params" => [
					"serviceName" => $this->serviceName,
					"headerID" => $responseToDedetrainingPrepHeader["id"],
				],
			]);
			/*DEBUG*/ //pr($promiseToDedetrainingStart->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Create Service
		$responseToDedetrainingStart = json_decode($promiseToDedetrainingStart->wait()->getBody()->getContents(), true);
		/*DEBUG*/ //pr($responseToDedetrainingStart);//exit;
	}
	
	public function predict() {
		$rates = $this->XeCurrency->getHistoricRates("USD", "SGD");
		
		//Reverse the rates
		$rRates = array_reverse($rates["list"]["86400"]["rates"]);
		/*DEBUG*/ //pr($rRates);
		
		//Data set
		$dataSet = $rRates[5]["rate"].",".
				$rRates[4]["rate"].",".
				$rRates[3]["rate"].",".
				$rRates[2]["rate"].",".
				$rRates[1]["rate"].",".
				$rRates[0]["rate"];
		
		//Async Init: Predict
		try {
			$promiseToDedepredictSingle = $this->dedeClient->postAsync("DeepDetect/predictSingle.json", [
				"form_params" => [
					"serviceName" => $this->serviceName,
					"dataHeader" => "day1,day2,day3,day4,day5,day6",
					"dataSet" => $dataSet,
				],
			]);
			/*DEBUG*/ //pr($promiseToDedepredictSingle->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Predict
		$responseToDedepredictSingle = json_decode($promiseToDedepredictSingle->wait()->getBody()->getContents(), true);
		/*DEBUG*/ pr($responseToDedepredictSingle);//exit;
		
		pr($rRates[0]);
	}
	
	public function awesomerTraining() {
		//If there is a docker running, stop it first.
		//Async Init: Docker Stop
		try {
			$promiseToDededockerDeepDetectStop = $this->dedeClient->postAsync("DeepDetect/dockerDeepDetectStop.json", [
				"form_params" => [
				],
			]);
			/*DEBUG*/ //pr($promiseToDededockerDeepDetectStop->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Docker Stop
		$responseToDededockerDeepDetectStop = json_decode($promiseToDededockerDeepDetectStop->wait()->getBody()->getContents(), true);
		/*DEBUG*/ //pr($responseToDededockerDeepDetectStop);//exit;
		
		//Start the docker
		//Async Init: Docker Start
		try {
			$promiseToDededockerDeepDetectStart = $this->dedeClient->postAsync("DeepDetect/dockerDeepDetectStart.json", [
				"form_params" => [
				],
			]);
			/*DEBUG*/ //pr($promiseToDededockerDeepDetectStart->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Docker Start
		$responseToDededockerDeepDetectStart = json_decode($promiseToDededockerDeepDetectStart->wait()->getBody()->getContents(), true);
		/*DEBUG*/ //pr($responseToDededockerDeepDetectStart);//exit;
		
		
		//It takes time for the docker to completely boot up
		sleep(10);
		
		
		
		$rates = $this->XeCurrency->getHistoricRates("USD", "SGD");
		for ($length = 2; $length <= 15; $length++) {

			//Async Init: Create Service
			try {
				$promiseToDedeservicesCreate = $this->dedeClient->postAsync("DeepDetect/servicesCreate.json", [
					"form_params" => [
						"service-name" => $this->serviceName."-".$length,
						"nclasses" => 3,
					],
				]);
				/*DEBUG*/ //pr($promiseToDedeservicesCreate->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
			
			
			
			
			
			$headers = "";
			for ($counter = 1; $counter <= $length; $counter++) {
				if ($counter == 1) {
					$headers = "day".$counter;
				} else {
					$headers .= ",day".$counter;
				}
			}
			//Async Init: Preparing Header
			try {
				$promiseToDedetrainingPrepHeader = $this->dedeClient->postAsync("DeepDetect/trainingPrepHeader.json", [
					"form_params" => [
						"headers" => $headers,
						"label" => "day".$length,
					],
				]);
				/*DEBUG*/ //pr($promiseToDedetrainingPrepHeader->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
			
			//Async Result: Preparing Header
			$responseToDedetrainingPrepHeader = json_decode($promiseToDedetrainingPrepHeader->wait()->getBody()->getContents(), true);
			/*DEBUG*/ //pr($responseToDedetrainingPrepHeader);//exit;
			
			
			
			
			
			
			foreach ($rates["list"]["86400"]["rates"] as $key => $rate) {
				//If there is no last day, it has reach the end.
				if (!isset($rates["list"]["86400"]["rates"][$key + ($length - 1)])) {
					break;
				}
					
				//Measure if the second last to last day is an increase or decrease
				//0 = decrease, 1 = equal, 2 = increase
				if ($rates["list"]["86400"]["rates"][$key + ($length - 2)]["rate"] > $rates["list"]["86400"]["rates"][$key + ($length - 1)]["rate"]) {
					$lastDay = 0;
				} else if ($rates["list"]["86400"]["rates"][$key + ($length - 2)]["rate"] == $rates["list"]["86400"]["rates"][$key + ($length - 1)]["rate"]) {
					$lastDay = 1;
				} else {
					$lastDay = 2;
				}
					
				//CSV Column
				$data = $rates["list"]["86400"]["rates"][$key]["rate"];
				for ($counter = 1; $counter < $length - 1; $counter++) {
					$data .= ",".$rates["list"]["86400"]["rates"][$key + $counter]["rate"];
				}
				$data .= ",".$lastDay;
							
				//Async Init: Preparing Data
				try {
					$promiseToDedetrainingPrepData = $this->dedeClient->postAsync("DeepDetect/trainingPrepData.json", [
						"form_params" => [
							"header-id" => $responseToDedetrainingPrepHeader["id"],
							"data" => $data,
						],
					]);
					/*DEBUG*/ //pr($promiseToDedetrainingPrepData->wait()->getBody()->getContents());exit;
				} catch (RequestException $e) {
					/*DEBUG*/ pr($e);exit;
				}
					
				//Async Result: Preparing Data
				$responseToDedetrainingPrepData = json_decode($promiseToDedetrainingPrepData->wait()->getBody()->getContents(), true);
				/*DEBUG*/ //pr($responseToDedetrainingPrepData);//exit;
			}
			
			
			


			//Async Result: Create Service
			$responseToDedeservicesCreate = json_decode($promiseToDedeservicesCreate->wait()->getBody()->getContents(), true);
			/*DEBUG*/ //pr($responseToDedeservicesCreate);//exit;
			
			
			
			
			//Async Init: Start Training
			try {
				$promiseToDedetrainingStart = $this->dedeClient->postAsync("DeepDetect/trainingStart.json", [
					"form_params" => [
						"serviceName" => $this->serviceName."-".$length,
						"headerID" => $responseToDedetrainingPrepHeader["id"],
					],
				]);
				/*DEBUG*/ //pr($promiseToDedetrainingStart->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
			
			//Async Result: Start Training
			$responseToDedetrainingStart = json_decode($promiseToDedetrainingStart->wait()->getBody()->getContents(), true);
			/*DEBUG*/ //pr($responseToDedetrainingStart);//exit;
		}
		
		
	}
	
	public function awesomerPredict() {
		$rates = $this->XeCurrency->getHistoricRates("USD", "SGD");
		/*DEBUG*/ //pr($rates);
		
		$today = new Chronos();
		
		//Get the last minute rate
		$count = count($rates["list"][60]["rates"]);

		$day = [];
		$day[1] = $rates["list"][60]["rates"][$count - 1]["rate"];
		$day[2] = $rates["list"][600]["rates"][0]["rate"];
		$day[3] = $rates["list"][900]["rates"][480]["rate"];
		$day[4] = $rates["list"][900]["rates"][384]["rate"];
		$day[5] = $rates["list"][900]["rates"][288]["rate"];
		$day[6] = $rates["list"][900]["rates"][192]["rate"];
		$day[7] = $rates["list"][900]["rates"][96]["rate"];
		$day[8] = $rates["list"][900]["rates"][0]["rate"];
		$day[9] = $rates["list"][3600]["rates"][553]["rate"];
		$day[10] = $rates["list"][3600]["rates"][529]["rate"];
		$day[11] = $rates["list"][3600]["rates"][505]["rate"];
		$day[12] = $rates["list"][3600]["rates"][481]["rate"];
		$day[13] = $rates["list"][3600]["rates"][457]["rate"];
		$day[14] = $rates["list"][3600]["rates"][433]["rate"];
		$day[15] = $rates["list"][3600]["rates"][409]["rate"];
		
		//Awesome Promises that doesn't require feedback
		$awesomePromises = [];
		
		for ($length = 2; $length <= 15; $length++) {
			
			$headers = "";
			for ($counter = 1; $counter < $length; $counter++) {
				if ($counter == 1) {
					$headers = "day".$counter;
				} else {
					$headers .= ",day".$counter;
				}
			}
			

			//CSV Column
			$data = [];
			for ($counter = $length - 1; $counter > 0; $counter--) {
				if ($counter == $length - 1) {
					$data = $day[$counter];
				} else {
					$data .= ",".$day[$counter];
				}
			}
			/*DEBUG*/ //pr($data);exit;
			
			
			//Async Init: Predict
			try {
				$promiseToDedepredictSingle = $this->dedeClient->postAsync("DeepDetect/predictSingle.json", [
					"form_params" => [
						"serviceName" => $this->serviceName."-".$length,
						"dataHeader" => $headers,
						"dataSet" => $data,
					],
				]);
				/*DEBUG*/ //pr($promiseToDedepredictSingle->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
			
			//Async Result: Predict
			$responseToDedepredictSingle = json_decode($promiseToDedepredictSingle->wait()->getBody()->getContents(), true);
			/*DEBUG*/ //pr($responseToDedepredictSingle);//exit;
			
			//Create the model name if it doesn't exist
			// else get the model for further processing
			if (!$this->SpeculativeModels->exists(["name" => $this->serviceName."-".$length])) {
				$dataToSave = [
					"name" => $this->serviceName."-".$length,
				];
				$model = $this->SpeculativeModels->newEntity($dataToSave);
				if (!$this->SpeculativeModels->save($model)) {
					/*DEBUG*/ //pr("Error Saving Model");pr($model);exit;
				}
			} else {
				$model = $this->SpeculativeModels->find("all", [
					"conditions" => [
						"name" => $this->serviceName."-".$length,
					],
				])
				->toArray();
				$model = $model[0];
			}
			/*DEBUG*/ //pr($model);
			
			//Find the last record and define the result
			$lastRecord = $this->SpeculativeModelRecords->find("all", [
				"conditions" => [
					"SpeculativeModelRecords.speculative_model_id" => $model->id,
				],
				"limit" => 1,
				"order" => [
					"SpeculativeModelRecords.date_of_last_rate" => "DESC",
					"SpeculativeModelRecords.id" => "DESC",
				],
			])
			->toArray();
			/*DEBUG*/ //pr($lastRecord);
			if (!empty($lastRecord) && empty($lastRecord[0]->result)) {
				$lastRecord[0]->actual_rate = $day[1];
				if ($day[1] > $lastRecord[0]->last_rate) {
					$state = 2;
				} else if ($day[1] == $lastRecord[0]->last_rate) {
					$state = 1;
				} else {
					$state = 0;
				}
				/*DEBUG*/ //pr($day[1]);
				/*DEBUG*/ //pr($lastRecord[0]->last_rate);
				/*DEBUG*/ //pr($state);
				
				if ($state == $lastRecord[0]->predicted_state) {
					$lastRecord[0]->result = true;
				} else {
					$lastRecord[0]->result = false;
				}
				/*DEBUG*/ //pr($lastRecord[0]->predicted_state);
				/*DEBUG*/ //pr($lastRecord[0]->result);
				
				$this->SpeculativeModelRecords->save($lastRecord[0]);

				//Async Init: Awesome Result
				try {
					$promiseToSpeculateinputDefineResult = $this->awesomeClient->postAsync("Speculate/inputDefineResult", [
						"form_params" => [
							"modelName" => $this->serviceName."-".$length,
							"result" => $lastRecord[0]->result,
							"actualRate" => $day[1],
						],
					]);
					/*DEBUG*/ //pr($promiseToSpeculateinputDefineResult->wait()->getBody()->getContents());exit;
				} catch (RequestException $e) {
					/*DEBUG*/ pr($e);exit;
				}
				
				//Async Result: Awesome Result
				$promiseToSpeculateinputDefineResult->wait();
				/*DEBUG*/ //pr($responseToSpeculateinputDefineResult);//exit;
			}
			
			//Add a new speculative record
			$dataToSave = [
				"speculative_model_id" => $model->id,
				"date_of_last_rate" => $today->toDateString(),
				"last_rate" => $day[1],
				"predicted_state" => $responseToDedepredictSingle["body"]["predictions"][0]["classes"][0]["cat"],
				"prediction_confidence" => $responseToDedepredictSingle["body"]["predictions"][0]["classes"][0]["prob"],
			];
			$newRecord = $this->SpeculativeModelRecords->newEntity($dataToSave);
			/*DEBUG*/ //pr($newRecord);
			if (!$this->SpeculativeModelRecords->save($newRecord)) {
				/*DEBUG*/ pr($newRecord);
			}
			
			//Async Push: Awesome New Input
			try {
				$awesomePromises[] = $this->awesomeClient->postAsync("Speculate/inputNewRecord", [
					"form_params" => [
						"modelName" => $this->serviceName."-".$length,
						"dateOfLastRate" => $today->toDateString(),
						"lastRate" => $day[1],
						"predictedState" => $responseToDedepredictSingle["body"]["predictions"][0]["classes"][0]["cat"],
						"predictionConfidence" => $responseToDedepredictSingle["body"]["predictions"][0]["classes"][0]["prob"],
					],
				]);
				/*DEBUG*/ //pr($promiseToDedepredictSingle->wait()->getBody()->getContents());exit;
			} catch (RequestException $e) {
				/*DEBUG*/ pr($e);exit;
			}
		}
		
		//Async Push completion
		if (!empty($awesomePromises)) {
			$results = Promise\unwrap($awesomePromises);
			/*DEBUG*/ //foreach ($results as $response) pr($response->getBody()->getContents());
		}
	}
	
}