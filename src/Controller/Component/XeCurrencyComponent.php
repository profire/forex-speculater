<?php
namespace Cake\Controller\Component;

use function GuzzleHttp\json_encode;
use function GuzzleHttp\json_decode;
use Cake\Chronos\Chronos;
use Cake\Controller\Component;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class XeCurrencyComponent extends Component {
	private $xeClient;
	
	public function beforeFilter() {
		//Use Guzzle as HTTP Client
		$this->xeClient = new Client([
			"base_uri" => "http://www.xe.com/",
			"timeout" => 3600,
			"default" => ["verify" => false,],
		]);
	}
	
	public function getHistoricRates($from, $to, $amount = 1, $extended = FALSE) {
		//To Extend or not to extend
		$toExtend = "";
		if ($extended) {
			$toExtend = "&extended=true";
		}
		
		//Async Init: Get Currency Historic Rates List
		try {
			$promiseToXeHistoricRatesList = $this->xeClient->postAsync("currencycharts/currates.php?from=".$from."&to=".$to."&amount=".$amount.$toExtend, [
				"form_params" => [
					"service-name" => "testservice",
				],
			]);
			/*DEBUG*/ //pr($promiseToXeHistoricRatesList->wait()->getBody()->getContents());exit;
		} catch (RequestException $e) {
			/*DEBUG*/ pr($e);exit;
		}
		
		//Async Result: Get Currency Historic Rates List
		$responseToXeHistoricRatesList = $promiseToXeHistoricRatesList->wait()->getBody()->getContents();
		/*DEBUG*/ //pr($responseToXeHistoricRatesList);//exit;
		
		$decryptedJSON = $this->decryptJSON($responseToXeHistoricRatesList);
		/*DEBUG*/ //pr($decryptedJSON);
		
		$historicRates = json_decode($decryptedJSON, true);
		/*DEBUG*/ //pr($historicRates);
		
		$dataToReturn = [
			"fromCurrency" => $historicRates[0]["from"],
			"toCurrency" => $historicRates[0]["to"]
		];
		
		foreach ($historicRates[0]["batchList"] as $list) {
			$interval = $list["interval"] / 1000;
			$dataToReturn["availableRates"][] = $interval;
			$dataToReturn["list"][$interval] = [];
			$dataToReturn["list"][$interval]["startDate"] = Chronos::createFromFormat("U", $list["startTime"]/1000);
			
			foreach ($list["rates"] as $key => $rate) {
				//Ignore first entry. It's another key to get accurate rates
				if ($key == 0) {
					continue;
				}
				$dataToReturn["list"][$interval]["rates"][] = [
					"dateTime" => $dataToReturn["list"][$interval]["startDate"]->addSeconds($interval * ($key - 1)),
					"rate" => $rate - $list["rates"][0],
				];
			}
		}
		
		return $dataToReturn;
	}
	
	private function decryptJSON($encryptedJSON) {
		//Trim any whitespaces
		$encryptedJSON = trim($encryptedJSON);
		
		//The decryption key is the last 4 characters of the encrypted JSON
		$hiddenKey = substr($encryptedJSON, strlen($encryptedJSON) - 4);
		/*DEBUG*/ //pr($hiddenKey);
		
		$decryptedKey = ord(substr($hiddenKey, 0)) + ord(substr($hiddenKey, 1)) + ord(substr($hiddenKey, 2)) + ord(substr($hiddenKey, 3));
		/*DEBUG*/ //pr($decryptedKey);
		
		$decryptedKey = (strlen($encryptedJSON) - 10) % $decryptedKey;
		/*DEBUG*/ //pr($decryptedKey);
		
		$decryptedKey = ($decryptedKey > strlen($encryptedJSON) - 10 - 4) ? (strlen($encryptedJSON) - 10 - 4) : $decryptedKey;
		/*DEBUG*/ //pr($decryptedKey);
		
		//The actual decryption key is hidden in the middle of the JSON
		$decryptedKey2 = substr($encryptedJSON, $decryptedKey, 10);
		/*DEBUG*/ //pr($decryptedKey2);
		
		//Remove the encryption key from the JSON string
		$encryptedJSON = substr($encryptedJSON, 0, $decryptedKey).substr($encryptedJSON, $decryptedKey + 10);
		/*DEBUG*/ //pr($encryptedJSON);
		
		//Decode URI
		$encryptedJSON = urldecode($encryptedJSON);
		/*DEBUG*/ //pr($encryptedJSON);
		
		
		
		
		
		//Character shift process, doesn't involve any key
		$stringList = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		$cursor = 0;
		$shiftedJSON = "";
		
		//Clear Unwanted characters
		$encryptedJSON = preg_replace("/[^A-Za-z0-9\+\/\=]/m", "", $encryptedJSON);
		/*DEBUG*/ //pr($encryptedJSON);
		
		do {
			$char1 = strpos($stringList, substr($encryptedJSON, $cursor++, 1));
			$char2 = strpos($stringList, substr($encryptedJSON, $cursor++, 1));
			$char3 = strpos($stringList, substr($encryptedJSON, $cursor++, 1));
			$char4 = strpos($stringList, substr($encryptedJSON, $cursor++, 1));
			/*DEBUG*/ //pr($char1);pr($char2);pr($char3);pr($char4);
				
			$code1 = ($char1 << 2) | ($char2 >> 4);
			$code2 = (($char2 & 15) << 4) | ($char3 >> 2);
			$code3 = (($char3 & 3) << 6) | $char4;
			/*DEBUG*/ //pr($code1);pr($code2);pr($code3);
				
			$shiftedJSON .= chr($code1);
			if ($char3 != 64) {
				$shiftedJSON .= chr($code2);
			}
			if ($char4 != 64) {
				$shiftedJSON .= chr($code3);
			}
		} while ($cursor < strlen($encryptedJSON));
		/*DEBUG*/ //pr($shiftedJSON);
		
		$encryptedJSON = urldecode($shiftedJSON);
		/*DEBUG*/ //pr($encryptedJSON);
		
		//Decrypt every 10th character
		$counter = 0;
		$decryptedJSON = "";
		for ($counter10th = 0; $counter10th < strlen($encryptedJSON); $counter10th+=10) {
			$encryptedChar = $encryptedJSON[$counter10th];
			/*DEBUG*/ //pr($encryptedChar);
				
			$shiftKey = $decryptedKey2[($counter % strlen($decryptedKey2) - 1) < 0 ? (strlen($decryptedKey2) + ($counter % strlen($decryptedKey2) - 1)) : ($counter % strlen($decryptedKey2) - 1)];
			/*DEBUG*/ //pr($shiftKey);
				
			$encryptedChar = chr(ord($encryptedChar) - ord($shiftKey));
			/*DEBUG*/ //pr($encryptedChar);
				
			$decryptedJSON .= $encryptedChar.substr($encryptedJSON, $counter10th + 1, 9);
			/*DEBUG*/ //pr($decryptedJSON);if ($counter10th == 20) break;
				
			$counter++;
		}
		/*DEBUG*/ //pr($decryptedJSON);
		
		return $decryptedJSON;
	}
	
}
